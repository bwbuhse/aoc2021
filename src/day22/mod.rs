use crate::lib::aoc;
use std::{
    collections::HashSet,
    hash::{Hash, Hasher},
    ops::RangeInclusive,
};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Status {
    On,
    Off,
}

impl Default for Status {
    fn default() -> Self {
        Status::Off
    }
}

#[derive(Debug, Copy, Clone, Eq, Default)]
struct Cube {
    // (x, y, z)
    coords: (isize, isize, isize),
    status: Status,
}

impl From<(isize, isize, isize)> for Cube {
    fn from(coords: (isize, isize, isize)) -> Self {
        Cube {
            coords,
            status: Status::Off,
        }
    }
}

impl PartialEq for Cube {
    fn eq(&self, other: &Self) -> bool {
        self.coords == other.coords
    }
}

impl Hash for Cube {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.coords.hash(state);
    }
}

impl Cube {
    #[inline]
    fn is_in_cuboid(
        &self,
        cuboid: &(
            RangeInclusive<isize>,
            RangeInclusive<isize>,
            RangeInclusive<isize>,
        ),
    ) -> bool {
        cuboid.0.contains(&self.coords.0)
            && cuboid.1.contains(&self.coords.1)
            && cuboid.2.contains(&self.coords.2)
    }
}

fn count_on(cubes: HashSet<Cube>) -> u32 {
    let mut count = 0;

    for cube in cubes {
        if cube.status == Status::On {
            count += 1;
        }
    }

    count
}

pub fn run() {
    let lines = aoc::get_lines("./inputs/day22.in");

    fn parse_cuboid_range(unparsed_range: &str) -> RangeInclusive<isize> {
        let split_range = unparsed_range
            .split_once("..")
            .expect("Invalid cuboid range!");

        let min = isize::max(
            split_range.0[2..]
                .parse()
                .expect("Failed to parse the min of a cuboid range!"),
            -50,
        );

        let max = isize::min(
            split_range
                .1
                .parse()
                .expect("Failed to parse the max of a cuboid range!"),
            50,
        );

        min..=max
    }

    let mut cubes = HashSet::new();
    for line in lines {
        let mut split_line = line.split_whitespace();
        let command = match split_line.next() {
            Some("on") => Status::On,
            Some("off") => Status::Off,
            _ => panic!("Invalid command! Line: {}", line),
        };

        let cuboid_ranges = match split_line.next() {
            Some(ranges) => ranges,
            None => panic!("Missing cuboid dimensions! Line: {}", line),
        };
        // Set up cuboid ranges for parsing
        let mut cuboid_ranges = cuboid_ranges.split(',');
        let x_range = parse_cuboid_range(cuboid_ranges.next().unwrap());
        let y_range = parse_cuboid_range(cuboid_ranges.next().unwrap());
        let z_range = parse_cuboid_range(cuboid_ranges.next().unwrap());
        let cuboid = (x_range.clone(), y_range.clone(), z_range.clone());

        for x in x_range {
            for y in y_range.clone() {
                for z in z_range.clone() {
                    let mut cube = cubes
                        .get(&Cube::from((x, y, z)))
                        .unwrap_or(&Cube::default())
                        .clone();
                    // println!(
                    //     "Cube: {:#?}, Cuboid: {:#?}, is_in_cuboid: {}",
                    //     cube,
                    //     cuboid,
                    //     cube.is_in_cuboid(&cuboid)
                    // );
                    cube.coords = (x, y, z);
                    cube.status = command;

                    if cube.is_in_cuboid(&cuboid) {
                        cubes.replace(cube);
                    }
                }
            }
        }
    }

    aoc::output(22, "num cubes on", count_on(cubes), 0);
}
