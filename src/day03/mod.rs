use crate::lib::aoc;

pub fn run() {
    let lines = aoc::get_lines("./inputs/day3.in");
    let num_lines = lines.len();
    let majority = (num_lines as f64 / 2.0) as u32;

    // Part 1
    let vec = vec![0; 12];
    let lines = lines
        .iter()
        .map(|line| {
            let mut line = line.split("").collect::<Vec<_>>();
            line.retain(|spot| !spot.is_empty());

            line.iter()
                .map(|spot| spot.parse::<u32>().expect("Not a number :c"))
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();
    let counts = lines.iter().fold(vec, |mut accum, line| {
        for i in 0..line.len() {
            accum[i] += line[i];
        }
        accum
    });

    let mut gamma = String::new();
    let mut epsilon = String::new();
    for count in counts.iter() {
        match count > &majority {
            true => {
                gamma += "1";
                epsilon += "0";
            }
            false => {
                gamma += "0";
                epsilon += "1";
            }
        }
    }

    let gamma = u32::from_str_radix(&gamma, 2).expect("Error converting String gamma to u32 gamma");
    let epsilon =
        u32::from_str_radix(&epsilon, 2).expect("Error converting String epsilon to u32 epsilon");
    let power_consumption = gamma * epsilon;

    // Part 2
    // 7908600 TOO HIGH
    // 1050225 TOO LOW
    let mut i = 0;
    let mut oxygen_lines = lines.clone();
    let mut majority = (num_lines as f64 / 2.0).ceil() as u32;
    loop {
        let mut count = 0;
        for line in &oxygen_lines {
            count += line[i];
        }

        if count >= majority {
            oxygen_lines.retain(|line| line[i] == 1);
        } else {
            oxygen_lines.retain(|line| line[i] == 0);
        }

        if oxygen_lines.len() <= 1 {
            break;
        }
        majority = (oxygen_lines.len() as f64 / 2.0).ceil() as u32;
        i += 1;
    }
    let mut oxygen = String::new();
    for spot in oxygen_lines[0].iter() {
        oxygen += &spot.to_string();
    }
    let oxygen =
        u32::from_str_radix(&oxygen, 2).expect("Error converting String oxygen to u32 oxygen");

    let mut i = 0;
    let mut co2_lines = lines;
    let mut majority = (num_lines as f64 / 2.0).ceil() as u32;
    loop {
        let mut count = 0;
        for line in &co2_lines {
            count += line[i];
        }

        if count >= majority {
            co2_lines.retain(|line| line[i] == 0);
        } else {
            co2_lines.retain(|line| line[i] == 1);
        }

        if co2_lines.len() <= 1 {
            break;
        }
        majority = (co2_lines.len() as f64 / 2.0).ceil() as u32;
        i += 1;
    }
    let mut co2 = String::new();
    for spot in co2_lines[0].iter() {
        co2 += &spot.to_string();
    }
    let co2 = u32::from_str_radix(&co2, 2).expect("Error converting String co2 to u32 co2");
    let life_support_rating = oxygen * co2;

    aoc::output(3, "multiplicand", power_consumption, life_support_rating);
}
