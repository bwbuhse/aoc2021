use crate::lib::aoc;
use std::{
    cmp::Ordering,
    collections::VecDeque,
    fmt::{Display, Formatter, Result},
    ops::{BitOr, BitOrAssign},
};

type Paper = Vec<Vec<PaperSpot>>;

#[derive(Debug, Copy, Clone)]
enum Fold {
    Up(usize),
    Left(usize),
    Invalid,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum PaperSpot {
    Dot,
    Empty,
}

impl Display for PaperSpot {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(
            f,
            "{}",
            match self {
                PaperSpot::Dot => '#',
                PaperSpot::Empty => '.',
            }
        )
    }
}

impl BitOr for PaperSpot {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        match self == PaperSpot::Dot || rhs == PaperSpot::Dot {
            true => PaperSpot::Dot,
            false => PaperSpot::Empty,
        }
    }
}

impl BitOrAssign for PaperSpot {
    fn bitor_assign(&mut self, rhs: Self) {
        match *self == PaperSpot::Dot || rhs == PaperSpot::Dot {
            true => *self = PaperSpot::Dot,
            false => *self = PaperSpot::Empty,
        }
    }
}

fn new_paper(num_rows: usize, num_cols: usize) -> Paper {
    vec![vec![PaperSpot::Empty; num_cols]; num_rows]
}

fn fold_paper(paper: Paper, fold: Fold) -> Paper {
    match fold {
        Fold::Up(fold_row) => {
            let mut folded_paper = new_paper(paper.len() / 2, paper[0].len());
            for row in 0..paper.len() {
                for col in 0..paper[row].len() {
                    match row.cmp(&fold_row) {
                        Ordering::Greater => {
                            folded_paper[paper.len() - 1 - row][col] |= paper[row][col]
                        }
                        Ordering::Less => folded_paper[row][col] |= paper[row][col],
                        Ordering::Equal => { /* If it's equal, this row is *on* the fold line so we can skip it*/
                        }
                    }
                }
            }
            folded_paper
        }
        Fold::Left(fold_col) => {
            let mut folded_paper = new_paper(paper.len(), paper[0].len() / 2);
            for row in 0..paper.len() {
                for col in 0..paper[row].len() {
                    match col.cmp(&fold_col) {
                        Ordering::Greater => {
                            folded_paper[row][paper[0].len() - 1 - col] |= paper[row][col]
                        }
                        Ordering::Less => folded_paper[row][col] |= paper[row][col],
                        Ordering::Equal => { /* If it's equal, this col is *on* the fold line so we can skip it*/
                        }
                    }
                }
            }
            folded_paper
        }
        Fold::Invalid => {
            eprintln!("Tried to fold an invalid fold! Ignoring...");
            paper.to_vec()
        }
    }
}

fn display_paper(paper: Paper) {
    for row in paper {
        for paper_spot in row {
            print!("{}", paper_spot)
        }
        println!()
    }
    println!()
}

fn count_dots(paper: Paper) -> u32 {
    let mut num_dots = 0;

    for row in paper {
        for paper_spot in row {
            if let PaperSpot::Dot = paper_spot {
                num_dots += 1
            };
        }
    }

    num_dots
}

pub fn run() {
    let lines = aoc::get_lines("./inputs/day13.in");

    let (num_rows, num_cols) =
        lines
            .iter()
            .take_while(|line| line.contains(','))
            .fold((0, 0), |(max_y, max_x), line| {
                let (x, y): (usize, usize) = match line.split_once(',') {
                    Some((x, y)) => (
                        x.parse().expect("Some x val is wrong!"),
                        y.parse().expect("Some y val is wrong!"),
                    ),
                    None => panic!("One of your dot lines is entered incorrectly!"),
                };

                // Add 1 to x and y since x and y are indices but max_x and max_y are number of
                // cols/rows
                (usize::max(y + 1, max_y), usize::max(x + 1, max_x))
            });

    let (paper, mut folds): (Paper, VecDeque<Fold>) = lines.iter().fold(
        (new_paper(num_rows, num_cols), VecDeque::new()),
        |(mut paper, mut folds), line| {
            if let Some((x, y)) = line.split_once(',') {
                // We already did the expects up there ^^
                let col: usize = x.parse().unwrap();
                let row: usize = y.parse().unwrap();
                paper[row][col] = PaperSpot::Dot;
            } else if line.starts_with("fold along") {
                if let Some((axis, index)) = line
                    .split(' ')
                    .nth(2)
                    .expect("One of your fold lines is entered incorrectly!")
                    .split_once('=')
                {
                    let index: usize = index.parse().unwrap();
                    let fold = match axis {
                        "y" => Fold::Up(index),
                        "x" => Fold::Left(index),
                        _ => {
                            eprintln!("Your input file has an invalid fold! Ignoring...");
                            Fold::Invalid
                        }
                    };

                    folds.push_back(fold);
                }
            }

            (paper, folds)
        },
    );

    // Do the first fold for part 1
    let paper_p1 = fold_paper(paper, folds.pop_front().expect("No folds in your input!"));

    // Finish folding then display the paper for part 2
    let mut paper = paper_p1.clone();
    while let Some(fold) = folds.pop_front() {
        paper = fold_paper(paper, fold);
    }

    aoc::output(13, "num dots", count_dots(paper_p1), 0);
    display_paper(paper);
    println!();
}
