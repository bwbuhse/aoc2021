#[derive(Clone, Copy, Debug)]
struct Row(u32, u32, u32, u32, u32);

#[derive(Clone, Copy, Debug)]
pub struct Board(Row, Row, Row, Row, Row);

impl Row {
    fn new(row: String) -> Self {
        let row = row.trim().split_whitespace().collect::<Vec<_>>();
        if row.len() != 5 {
            panic!("Tried to read a row with len != 5");
        }

        Row(
            row[0]
                .parse::<u32>()
                .expect("Error reading row 0 (somewhere)"),
            row[1]
                .parse::<u32>()
                .expect("Error reading row 1 (somewhere)"),
            row[2]
                .parse::<u32>()
                .expect("Error reading row 2 (somewhere)"),
            row[3]
                .parse::<u32>()
                .expect("Error reading row 3 (somewhere)"),
            row[4]
                .parse::<u32>()
                .expect("Error reading row 4 (somewhere)"),
        )
    }

    fn check_for_num(&mut self, num: u32) {
        if self.0 == num {
            self.0 = 0;
        }
        if self.1 == num {
            self.1 = 0;
        }
        if self.2 == num {
            self.2 = 0;
        }
        if self.3 == num {
            self.3 = 0;
        }
        if self.4 == num {
            self.4 = 0;
        }
    }

    fn get_score(&mut self) -> u32 {
        self.0 + self.1 + self.2 + self.3 + self.4
    }
}

impl Board {
    pub fn new(rows: Vec<String>) -> Self {
        if rows.len() != 5 {
            panic!("Tried to read a board with len != 5");
        }

        Board(
            Row::new(rows[0].to_owned()),
            Row::new(rows[1].to_owned()),
            Row::new(rows[2].to_owned()),
            Row::new(rows[3].to_owned()),
            Row::new(rows[4].to_owned()),
        )
    }

    pub fn check_for_num(&mut self, num: u32) {
        self.0.check_for_num(num);
        self.1.check_for_num(num);
        self.2.check_for_num(num);
        self.3.check_for_num(num);
        self.4.check_for_num(num);
    }

    fn check_diags(&mut self) -> bool {
        self.0 .0 + self.1 .1 + self.2 .2 + self.3 .3 + self.4 .4 == 0
            || self.0 .4 + self.1 .3 + self.2 .2 + self.3 .1 + self.4 .4 == 0
    }

    fn check_verticals(&mut self) -> bool {
        self.0 .0 + self.1 .0 + self.2 .0 + self.3 .0 + self.4 .0 == 0
            || self.0 .1 + self.1 .1 + self.2 .1 + self.3 .1 + self.4 .1 == 0
            || self.0 .2 + self.1 .2 + self.2 .2 + self.3 .2 + self.4 .2 == 0
            || self.0 .3 + self.1 .3 + self.2 .3 + self.3 .3 + self.4 .3 == 0
            || self.0 .4 + self.1 .4 + self.2 .4 + self.3 .4 + self.4 .4 == 0
    }

    pub fn has_won(&mut self) -> bool {
        if self.0.get_score() == 0
            || self.1.get_score() == 0
            || self.2.get_score() == 0
            || self.3.get_score() == 0
            || self.4.get_score() == 0
            || self.check_diags()
            || self.check_verticals()
        {
            return true;
        }

        false
    }

    pub fn get_score(&mut self) -> u32 {
        self.0.get_score()
            + self.1.get_score()
            + self.2.get_score()
            + self.3.get_score()
            + self.4.get_score()
    }
}

impl PartialEq for Row {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
            && self.1 == other.1
            && self.2 == other.2
            && self.3 == other.3
            && self.4 == other.4
    }
}

impl Eq for Row {}

impl PartialEq for Board {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
            && self.1 == other.1
            && self.2 == other.2
            && self.3 == other.3
            && self.4 == other.4
    }
}

impl Eq for Board {}
