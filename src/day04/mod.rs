use crate::day04::board::Board;
use crate::lib::aoc;

mod board;

pub fn run() {
    let lines = aoc::get_lines("./inputs/day4.in");

    // Split the numbers off
    let numbers = lines[0]
        .split(',')
        .map(|number| {
            number
                .parse::<u32>()
                .expect("Unable to read some number as a u32")
        })
        .collect::<Vec<_>>();

    // Get just the boards
    let lines = lines
        .get(2..)
        .expect("Did you forget to include boards in the input?");
    let mut boards: Vec<Board> = Vec::new();
    for i in (0..lines.len()).step_by(6) {
        boards.push(Board::new(
            lines
                .get(i..(i + 5))
                .expect("There was an issue slicing your board!")
                .to_vec(),
        ));
    }

    let mut first_win = false;
    let mut winning_number: u32 = 0;
    let mut winning_score: u32 = 0;
    let mut losing_number: u32 = 0;
    let mut losing_score: u32 = 0;
    // TODO: Is there a way to make this loop more idiomatic?
    for number in numbers {
        let mut winning_boards: Vec<Board> = Vec::new();
        for board in &mut boards {
            board.check_for_num(number);

            if board.has_won() {
                if !first_win {
                    first_win = true;
                    winning_number = number;
                    winning_score = board.get_score();
                }
                winning_boards.push(*board);
            }
        }

        boards.retain(|board| !winning_boards.contains(board));

        if boards.is_empty() {
            losing_number = number;
            losing_score = winning_boards[0].get_score();
        }

        if losing_score > 0 {
            break;
        }
    }

    aoc::output(
        4,
        "score",
        winning_number * winning_score,
        losing_number * losing_score,
    );
}
