use crate::lib::aoc;
use std::collections::HashMap;

fn do_steps(
    rules: &HashMap<String, char>,
    polymer: HashMap<(char, char), u64>,
    mut ends_with: (char, char),
) -> (HashMap<(char, char), u64>, (char, char)) {
    let mut new_polymer = HashMap::new();
    for (pair, count) in &polymer {
        let new_char = *rules
            .get(&format!("{}{}", pair.0, pair.1))
            .expect("Missing some rule!");

        // Now we can make two pairs
        let new_pair1 = (pair.0, new_char);
        let new_pair2 = (new_char, pair.1);
        new_polymer.insert(new_pair1, new_polymer.get(&new_pair1).unwrap_or(&0) + count);
        new_polymer.insert(new_pair2, new_polymer.get(&new_pair2).unwrap_or(&0) + count);

        if *pair == ends_with {
            ends_with = new_pair2;
        }
    }
    (new_polymer, ends_with)
}

fn get_result(polymer: &HashMap<(char, char), u64>, ends_with: &(char, char)) -> u64 {
    let mut count_vec: Vec<u64> = polymer
        .iter()
        .fold(
            HashMap::<char, u64>::new(),
            |mut accum, ((ith_char, jth_char), pair_count)| {
                let ith_char_count = accum.get(ith_char).unwrap_or(&0) + pair_count;
                let jth_char_count = accum.get(jth_char).unwrap_or(&0) + 1;

                if (*ith_char, *jth_char) == *ends_with {
                    accum.insert(*ith_char, ith_char_count);
                    accum.insert(*jth_char, jth_char_count);
                } else {
                    accum.insert(*ith_char, ith_char_count);
                }

                accum
            },
        )
        .values()
        .cloned()
        .collect();
    count_vec.sort_unstable();

    count_vec.last().unwrap() - count_vec[0]
}

pub fn run() {
    // Parse the input
    let lines = aoc::get_lines("./inputs/day14.in");
    let template = lines[0].clone();
    let rules: HashMap<String, char> = lines[2..].iter().fold(HashMap::new(), |mut rules, line| {
        if let Some((pair, insertion)) = line.split_once(" -> ") {
            rules.insert(
                pair.to_owned(),
                insertion.chars().next().expect("Some rule is invalid!"),
            );
        }
        rules
    });

    // Set up the polymer HashMap
    let mut template_chars = template.chars().peekable();
    let mut polymer: HashMap<(char, char), u64> = HashMap::new();
    let mut ends_with: (char, char) = ('0', '0');
    for i in 0..template.len() - 1 {
        let ith_char = template_chars.next().unwrap();
        let jth_char = *template_chars.peek().unwrap();
        let pair = (ith_char, jth_char);
        polymer.insert(pair, polymer.get(&pair).unwrap_or(&0) + 1);

        // We need to keep track of the ending pairs so that we can count chars correctly
        if i == template.len() - 2 {
            ends_with = pair;
        }
    }

    // ~~~ Part 1 ~~~
    for _ in 0..10 {
        let (new_polymer, new_ends_with) = do_steps(&rules, polymer.clone(), ends_with);
        polymer = new_polymer;
        ends_with = new_ends_with;
    }

    let result_p1 = get_result(&polymer, &ends_with);

    // ~~~ Part 2 ~~~
    for _ in 10..40 {
        let (new_polymer, new_ends_with) = do_steps(&rules, polymer.clone(), ends_with);
        polymer = new_polymer;
        ends_with = new_ends_with;
    }
    let result_p2 = get_result(&polymer, &ends_with);

    aoc::big_output(14, "result", result_p1, result_p2);
}
