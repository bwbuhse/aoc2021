use std::collections::{HashMap, HashSet, VecDeque};
use std::fmt::{Display, Formatter, Result};
use std::hash::{Hash, Hasher};

#[derive(Clone, Debug, Eq)]
struct Node {
    name: String,
    is_big: bool,
    adjacency_list: HashSet<String>,
}

#[derive(Clone, Debug)]
pub struct Graph {
    nodes: HashMap<String, Node>,
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name && self.is_big == other.is_big
    }
}

impl Hash for Node {
    fn hash<H: Hasher>(&self, state: &mut H) {
        // We're too lazy to hash the adjacency_list, so whatevs
        self.name.hash(state);
        self.is_big.hash(state);
    }
}

impl From<&str> for Node {
    fn from(name: &str) -> Self {
        Node {
            name: name.to_owned(),
            is_big: name
                .chars()
                .next()
                .expect("Empty string passed to Node::from!")
                .is_uppercase(),
            adjacency_list: HashSet::new(),
        }
    }
}

impl From<Vec<String>> for Graph {
    fn from(lines: Vec<String>) -> Self {
        let mut nodes: HashMap<String, Node> = HashMap::new();

        for line in lines {
            // "from" and "to" are just symbolic since this is a non-directional graph
            let (from_str, to_str) = line
                .split_once('-')
                .expect("Could not split some line! Is your input correct?");

            // If "from" exists in nodes, add "to" to its adjacency list, otherwise create from and
            // do the same. Repeat for "to"
            let mut from_node = match nodes.get_mut(from_str) {
                Some(node) => node.clone(),
                None => Node::from(from_str),
            };
            let mut to_node = match nodes.get_mut(to_str) {
                Some(node) => node.clone(),
                None => Node::from(to_str),
            };

            from_node.add_neigbor(to_str.to_string());
            to_node.add_neigbor(from_str.to_string());

            nodes.insert(from_str.to_string(), from_node);
            nodes.insert(to_str.to_string(), to_node);
        }

        Graph { nodes }
    }
}

impl Node {
    fn add_neigbor(&mut self, neighbor: String) {
        self.adjacency_list.replace(neighbor);
    }
}

impl Graph {
    pub fn count_paths(&self, max_visits: u32) -> u32 {
        // Do a BFS
        // The queue has to hold the current node, the path to that node, and if a small cave has
        //      been visited twice
        let mut paths: HashSet<String> = HashSet::new();
        let mut queue: VecDeque<(String, String, bool)> =
            VecDeque::from([("start".to_owned(), "start".to_owned(), false)]);

        while let Some((node_str, path, visited_twice)) = queue.pop_front() {
            if node_str == "end" {
                paths.insert(path);
                continue;
            }

            let node = self
                .nodes
                .get(&node_str)
                .expect("Some node is missing from the graph!");

            for neighbor_str in &node.adjacency_list {
                let new_path = format!("{},{}", path, neighbor_str);
                let neighbor_node = self
                    .nodes
                    .get(neighbor_str)
                    .expect("Some node is missing from the graph!");

                if neighbor_node.is_big {
                    queue.push_back((neighbor_str.to_string(), new_path, visited_twice));
                } else if neighbor_str != "start" {
                    let num_visits =
                        path.split(',')
                            .fold(0, |accum, cur| match cur == neighbor_str {
                                true => accum + 1,
                                false => accum,
                            });

                    if num_visits < 1 || (num_visits < max_visits && !visited_twice) {
                        queue.push_back((
                            neighbor_str.to_string(),
                            new_path,
                            visited_twice || num_visits == 1,
                        ));
                    }
                }
            }
        }

        let mut paths: Vec<String> = paths.iter().cloned().collect();
        paths.sort();
        paths.len() as u32
    }
}

impl Display for Node {
    fn fmt(&self, f: &mut Formatter) -> Result {
        if let Err(e) = write!(
            f,
            "Name: {}, is big: {}, neighbors: ",
            self.name, self.is_big
        ) {
            return Err(e);
        }
        for neighbor in &self.adjacency_list {
            if let Err(e) = write!(f, "{} ", neighbor) {
                return Err(e);
            }
        }

        Ok(())
    }
}

impl Display for Graph {
    fn fmt(&self, f: &mut Formatter) -> Result {
        if let Err(e) = writeln!(f, "~~~ Graph ~~~") {
            return Err(e);
        }

        for node in self.nodes.values() {
            if let Err(e) = writeln!(f, "{}", node) {
                return Err(e);
            }
        }

        writeln!(f)
    }
}
