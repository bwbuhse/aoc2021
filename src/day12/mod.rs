pub mod graph;
use crate::lib::aoc;
use graph::Graph;

pub fn run() {
    let graph = Graph::from(aoc::get_lines("./inputs/day12.in"));
    let num_paths_p1 = graph.count_paths(1);
    let num_paths_p2 = graph.count_paths(2);

    aoc::output(12, "num_paths", num_paths_p1, num_paths_p2);
}
