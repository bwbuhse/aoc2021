use crate::lib::aoc;

pub fn run() {
    let lines = aoc::get_lines("./inputs/day10.in");

    let mut error_score = 0;
    let mut completion_scores: Vec<u64> = Vec::new();
    for line in lines {
        let mut stack: Vec<char> = Vec::new();
        let mut is_valid = true;
        // Check if the line is valid
        for c in line.chars() {
            match c {
                '(' | '[' | '{' | '<' => stack.push(c),
                ')' => match stack.pop() {
                    Some('(') => { /* This is what we want but there's nothingto do */ }
                    _ => {
                        error_score += 3;
                        is_valid = false
                    }
                },
                ']' => match stack.pop() {
                    Some('[') => { /* This is what we want but there's nothingto do */ }
                    _ => {
                        error_score += 57;
                        is_valid = false
                    }
                },
                '}' => match stack.pop() {
                    Some('{') => { /* This is what we want but there's nothingto do */ }
                    _ => {
                        error_score += 1197;
                        is_valid = false
                    }
                },
                '>' => match stack.pop() {
                    Some('<') => { /* This is what we want but there's nothingto do */ }
                    _ => {
                        error_score += 25137;
                        is_valid = false
                    }
                },
                _ => eprintln!("Non-bracket character ({}) found! Ignoring...", c),
            }
        }
        // If it is valid, get the line's completion score
        if is_valid {
            let mut completion_score = 0;
            for c in stack.iter().rev() {
                completion_score *= 5;
                match c {
                    '(' => completion_score += 1,
                    '[' => completion_score += 2,
                    '{' => completion_score += 3,
                    '<' => completion_score += 4,
                    _ => {
                        eprintln!("Non-opening-bracket character ({}) found! Ignoring...", c);
                        // Unmultiply the score
                        completion_score /= 5;
                    }
                }
            }
            completion_scores.push(completion_score);
        }
    }
    completion_scores.sort_unstable();

    aoc::big_output(
        10,
        "score",
        error_score,
        completion_scores[completion_scores.len() / 2],
    );
}
