use crate::lib::aoc;

pub fn run() {
    let lines = aoc::get_lines("./inputs/day2.in");

    // Part 1 values
    let mut depth_1 = 0;
    let mut horizontal_pos_1 = 0;

    // Part 2 values
    let mut depth_2 = 0;
    let mut horizontal_pos_2 = 0;
    let mut aim = 0;

    for line in lines {
        let split = line.split(' ').collect::<Vec<_>>();

        let command = split[0];
        let value = split[1].parse::<u32>().unwrap();

        // Part 1
        match command {
            "forward" => horizontal_pos_1 += value,
            "down" => depth_1 += value,
            "up" => depth_1 -= value,
            _ => println!("{} is not a valid command", command),
        }

        // Part 2
        match command {
            "forward" => {
                horizontal_pos_2 += value;
                depth_2 += aim * value;
            }
            "down" => aim += value,
            "up" => aim -= value,
            _ => println!("{} is not a valid command", command),
        }
    }

    aoc::output(
        2,
        "multiplicand",
        depth_1 * horizontal_pos_1,
        depth_2 * horizontal_pos_2,
    );
}
