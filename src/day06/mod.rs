use crate::lib::aoc;

pub fn run() {
    let mut fish: [u64; 9] = aoc::get_lines("./inputs/day6.in")[0]
        .split(',')
        .map(|num| num.parse::<usize>().expect("Unable to read some number"))
        .fold([0; 9], |mut fishes, fish| {
            fishes[fish] += 1;
            fishes
        });

    for _ in 0..80 {
        let save = fish[0];
        fish[0] = fish[1];
        fish[1] = fish[2];
        fish[2] = fish[3];
        fish[3] = fish[4];
        fish[4] = fish[5];
        fish[5] = fish[6];
        fish[6] = fish[7] + save;
        fish[7] = fish[8];
        fish[8] = save;
    }

    let sum_p1: u64 = fish.iter().sum();

    for _ in 80..256 {
        let save = fish[0];
        fish[0] = fish[1];
        fish[1] = fish[2];
        fish[2] = fish[3];
        fish[3] = fish[4];
        fish[4] = fish[5];
        fish[5] = fish[6];
        fish[6] = fish[7] + save;
        fish[7] = fish[8];
        fish[8] = save;
    }

    let sum_p2: u64 = fish.iter().sum();

    aoc::big_output(6, "number of fish", sum_p1, sum_p2);
}
