use std::env;
mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day22;
mod lib;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("The day to execute *must* be passed as an argument!");
    }

    let day: u32 = args[1].parse().unwrap();

    match day {
        1 => day01::run(),
        2 => day02::run(),
        3 => day03::run(),
        4 => day04::run(),
        5 => day05::run(),
        6 => day06::run(),
        7 => day07::run(),
        8 => day08::run(),
        9 => day09::run(),
        10 => day10::run(),
        11 => day11::run(),
        12 => day12::run(),
        13 => day13::run(),
        14 => day14::run(),
        15 => day15::run(),
        16 => day16::run(),
        17 => day17::run(),
        18 => day18::run(),
        22 => day22::run(),
        _ => eprintln!("Tried to execute an invalid/not yet implemented day number!"),
    }
}
