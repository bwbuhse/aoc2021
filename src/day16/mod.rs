mod packet;
use crate::lib::aoc;
use hex::FromHex;
use packet::Packet;

fn sum_versions(packet: &Packet) -> u32 {
    let mut sum = *packet.version() as u32;

    if let Some(sub_packets) = packet.sub_packets() {
        for sub_packet in sub_packets {
            sum += sum_versions(sub_packet);
        }
    }

    sum
}

pub fn run() {
    let transmission_bytes = Vec::from_hex(aoc::get_lines("./inputs/day16.in")[0].clone()).unwrap();
    let transmission_bytes_string = transmission_bytes
        .iter()
        .fold(String::new(), |accum, cur| format!("{}{:08b}", accum, cur));
    let packet = Packet::from(transmission_bytes_string);

    let sum_of_versions = sum_versions(&packet);

    aoc::big_output(16, "output", sum_of_versions.into(), packet.value());
}
