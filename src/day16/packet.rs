use std::fmt::{Display, Formatter, Result};
#[derive(Debug, Clone, Copy)]
enum PacketType {
    Literal,
    Operator(u8),
}

impl From<u8> for PacketType {
    fn from(bits: u8) -> Self {
        match bits {
            0..=3 | 5..=7 => PacketType::Operator(bits),
            4 => PacketType::Literal,
            _ => panic!("Invalid packet type value!"),
        }
    }
}

impl Display for PacketType {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            PacketType::Literal => write!(f, "Literal"),
            PacketType::Operator(op) => write!(f, "Operator({})", op),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Packet {
    version: u8,
    packet_type: PacketType,
    literal: Option<String>,
    sub_packets: Option<Vec<Packet>>,
}

impl Packet {
    pub fn version(&self) -> &u8 {
        &self.version
    }

    pub fn sub_packets(&self) -> &Option<Vec<Packet>> {
        &self.sub_packets
    }

    /// Get the value of the packet based on its packet_type
    pub fn value(&self) -> u64 {
        match self.packet_type {
            PacketType::Literal => u64::from_str_radix(self.literal.as_ref().unwrap(), 2)
                .expect("Invalid literal payload!"),
            PacketType::Operator(op) => {
                let sub_packets = self
                    .sub_packets
                    .as_ref()
                    .expect("Operator packet with no sub packets!");
                match op {
                    0 => sub_packets
                        .iter()
                        .fold(0, |accum, packet| accum + packet.value()),
                    1 => sub_packets
                        .iter()
                        .fold(1, |accum, packet| accum * packet.value()),
                    2 => sub_packets.iter().fold(u64::max_value(), |accum, packet| {
                        u64::min(accum, packet.value())
                    }),
                    3 => sub_packets.iter().fold(u64::min_value(), |accum, packet| {
                        u64::max(accum, packet.value())
                    }),
                    5 => match sub_packets[0].value() > sub_packets[1].value() {
                        true => 1,
                        false => 0,
                    },
                    6 => match sub_packets[0].value() < sub_packets[1].value() {
                        true => 1,
                        false => 0,
                    },
                    7 => match sub_packets[0].value() == sub_packets[1].value() {
                        true => 1,
                        false => 0,
                    },
                    _ => panic!("Operator packet with invalid opcode!!"),
                }
            }
        }
    }
}

impl From<String> for Packet {
    fn from(bytes_string: String) -> Self {
        fn new_packet(bytes_string: &str) -> (Packet, usize) {
            // All packets start with a standard 6-bit header
            // First 3 bits are the version, next 3 are the packet type
            let version = u8::from_str_radix(&bytes_string[0..3], 2)
                .expect("Failed to parse a packet's version num!");
            let packet_type_num = u8::from_str_radix(&bytes_string[3..6], 2)
                .expect("Failed to parse a packet's type num!");
            let packet_type = PacketType::from(packet_type_num);

            let mut idx = 6; // Because the header is always 6 bits, we can start it at 6

            /// Get the payload (a vec of sub_packets) for operators packets
            fn get_operator_payload(
                length_type_id: u8,
                bytes_string: &str,
            ) -> (Vec<Packet>, usize) {
                let mut sub_packets = Vec::new();
                let mut idx;
                let length_range = match length_type_id {
                    0 => {
                        // Type ID 0: next 15 bits is the length in bits of the sub_packets
                        idx = 15;
                        0..idx
                    }
                    1 => {
                        // Type ID 1: next 11 bits is the number of sub_packets
                        idx = 11;
                        0..idx
                    }
                    _ => panic!("Invalid length_type_id!"),
                };

                let length = usize::from_str_radix(&bytes_string[length_range], 2)
                    .expect("Some packet's length type is invalid!");
                match length_type_id {
                    0 => {
                        // Type ID 0
                        while idx < 15 + length {
                            let new_bytes_string = &bytes_string[idx..15 + length];
                            if new_bytes_string.len() > 6 {
                                // We want to make sure to ignore leftover bits
                                let (packet, new_idx) = new_packet(&bytes_string[idx..15 + length]);
                                sub_packets.push(packet);
                                idx += new_idx;
                            } else {
                                break;
                            }
                        }
                        idx = 15 + length + 1;
                    }
                    1 => {
                        // Type ID 1
                        while sub_packets.len() < length {
                            let (packet, new_idx) = new_packet(&bytes_string[idx..]);
                            sub_packets.push(packet);
                            idx += new_idx;
                        }
                        idx += 1;
                    }
                    _ => panic!("Invalid length_type_id!"),
                }

                (sub_packets, usize::min(idx, bytes_string.len()))
            }

            /// Get the payload (a literal number) for literal packets
            fn get_literal_payload(bytes_string: &str) -> (String, usize) {
                let mut idx = 0;
                let mut literal = String::new();
                // Literals are groups of 5 bits
                // The first bit is a prefix and the next 4 are bits of the literal in big-endian
                //      format.
                // If the prefix is 1, there are more groups. If the prefix is 0, it's the last
                //      group.
                while bytes_string[idx..(idx + 1)] == *"1" {
                    literal = format!(
                        "{}{}",
                        literal,
                        bytes_string[(idx + 1)..(idx + 5)].to_owned()
                    );
                    idx += 5;
                }
                // Need to add 5 one more time since there are still 4 bits after the 0 prefix
                literal = format!(
                    "{}{}",
                    literal,
                    bytes_string[(idx + 1)..usize::min(idx + 5, bytes_string.len())].to_owned()
                );
                idx += 5;
                (literal, idx)
            }

            // The rest of bytes_string can either be a literal value or sub-packets belonging
            // to this packet
            let literal;
            let sub_packets;
            match packet_type {
                PacketType::Literal => {
                    let (payload, new_idx) = get_literal_payload(&bytes_string[idx..]);
                    literal = Some(payload);
                    idx += new_idx;
                    sub_packets = None;
                }
                PacketType::Operator(_) => {
                    let length_type_id = u8::from_str_radix(&bytes_string[6..7], 2).unwrap();
                    let (sub_pckts, new_idx) =
                        get_operator_payload(length_type_id, &bytes_string[7..]);
                    idx += new_idx;
                    sub_packets = Some(sub_pckts);
                    literal = None;
                }
            }

            (
                Packet {
                    version,
                    packet_type,
                    literal,
                    sub_packets,
                },
                idx,
            )
        }

        let (packet, _) = new_packet(&bytes_string);
        packet
    }
}

impl Display for Packet {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self.packet_type {
            PacketType::Literal => write!(
                f,
                "Version: {}, Type: {}, Payload: {}",
                self.version,
                self.packet_type,
                self.literal.as_ref().unwrap(),
            ),
            PacketType::Operator(_) => write!(
                f,
                "Version: {}, Type: {}, Payload: {:#?}",
                self.version, self.packet_type, self.sub_packets,
            ),
        }
    }
}
