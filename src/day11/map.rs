use std::fmt::{Display, Formatter, Result};

#[derive(Debug)]
struct Row {
    row: Vec<u32>,
}

#[derive(Debug)]
pub struct Map {
    rows: Vec<Row>,
    num_flashes: u32,
}

impl From<&String> for Row {
    fn from(row: &String) -> Self {
        let row = row.chars().fold(Vec::new(), |mut accum, cur| {
            accum.push(cur.to_digit(10).expect("Failed to convert a char to u32!"));
            accum
        });

        Row { row }
    }
}

impl From<Vec<String>> for Map {
    fn from(lines: Vec<String>) -> Self {
        let rows = lines.iter().map(Row::from).collect();

        Map {
            rows,
            num_flashes: 0,
        }
    }
}

impl Display for Row {
    fn fmt(&self, f: &mut Formatter) -> Result {
        for octopus in &self.row {
            if let Err(e) = write!(f, "{}", octopus) {
                return Err(e);
            }
        }
        writeln!(f)
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut Formatter) -> Result {
        for row in &self.rows {
            if let Err(e) = write!(f, "{}", row) {
                return Err(e);
            }
        }
        writeln!(f)
    }
}

impl Row {
    fn len(&self) -> usize {
        self.row.len()
    }

    /// Returns a Vec of any indices that hit 9, i.e. indices that will flash
    fn step(&mut self) -> Vec<usize> {
        let mut flashing_cols = Vec::new();

        for col in 0..self.len() {
            self.row[col] += 1;
            if self.row[col] == 10 {
                flashing_cols.push(col);
            }
        }
        flashing_cols
    }

    /// Returns a Vec of any indices that hit 9, i.e. indices that will flash
    fn flash(&mut self, col: isize) -> Vec<usize> {
        let mut flashing_cols = Vec::new();

        if col >= 0 && col < self.len() as isize {
            let col = col as usize;
            self.row[col] += 1;
            if self.row[col] == 10 {
                flashing_cols.push(col);
            }
        }

        flashing_cols
    }

    /// Zero-out a flashed coordinate
    fn reset_coord(&mut self, col: usize) {
        self.row[col] = 0;
    }
}

impl Map {
    /**
     * Complete one step of the map.
     *
     * Returns an bool which is true if the flashes synchronize, else false
     **/
    pub fn step(&mut self) -> bool {
        let mut flashing_coords: Vec<(usize, usize)> = Vec::new();
        let mut flashed_coords: Vec<(usize, usize)> = Vec::new();

        // Get any coords that flash during the initial step
        for row in 0..self.rows.len() {
            let flashing_cols = self.rows[row].step();
            self.num_flashes += flashing_cols.len() as u32;
            let mut flashing_cols = flashing_cols.iter().map(|col| (row, *col)).collect();
            flashing_coords.append(&mut flashing_cols);
        }

        // While we still have newly flashed coords, flash its neighbors
        while let Some((row, col)) = flashing_coords.pop() {
            flashed_coords.push((row, col));

            for row_offset in [-1, 0, 1] {
                if row as isize + row_offset >= 0
                    && row as isize + row_offset < self.rows.len() as isize
                {
                    let row = (row as isize + row_offset) as usize;
                    for col_offset in [-1, 0, 1] {
                        let flashing_cols = self.rows[row].flash(col as isize + col_offset);
                        self.num_flashes += flashing_cols.len() as u32;
                        let mut flashing_cols =
                            flashing_cols.iter().map(|col| (row, *col)).collect();
                        flashing_coords.append(&mut flashing_cols);
                    }
                }
            }
        }

        for (row, col) in &flashed_coords {
            self.rows[*row].reset_coord(*col);
        }

        // We can assume that the map is always a rectangle, so the flashes synchronize if
        // flashed_coords.len() == num rows * num cols
        flashed_coords.len() == self.rows.len() * self.rows[0].len()
    }

    pub fn get_num_flashes(&self) -> u32 {
        self.num_flashes
    }
}
