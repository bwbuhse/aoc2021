mod map;
use crate::lib::aoc;
use map::Map;

pub fn run() {
    let mut map = Map::from(aoc::get_lines("./inputs/day11.in"));

    let mut first_synchronization: u32 = 0;
    let mut first_synchronization_found = false;
    // We need specifically through 100 steps for part 1
    for i in 0..100 {
        first_synchronization_found = map.step();
        if first_synchronization_found {
            first_synchronization = i;
        }
    }
    let num_flashes_p1 = map.get_num_flashes();

    if !first_synchronization_found {
        for i in 101.. {
            first_synchronization_found = map.step();
            if first_synchronization_found {
                first_synchronization = i;
                break;
            }
        }
    }

    aoc::output(11, "num flashes", num_flashes_p1, first_synchronization);
}
