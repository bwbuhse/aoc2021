use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn get_lines(filename: &str) -> Vec<String> {
    let f = File::open(filename).expect("Something went wrong opening the file.");
    let reader = BufReader::new(f);

    reader
        .lines()
        .collect::<Result<_, _>>()
        .expect("Something while collecting the lines")
}

pub fn output(day: u32, text: &str, result_p1: u32, result_p2: u32) {
    println!("~~~ Day {} ~~~", day);
    println!("Part 1 - {}: {}", text, result_p1);
    println!("Part 2 - {}: {}\n", text, result_p2);
}

pub fn big_output(day: u32, text: &str, result_p1: u64, result_p2: u64) {
    println!("~~~ Day {} ~~~", day);
    println!("Part 1 - {}: {}", text, result_p1);
    println!("Part 2 - {}: {}\n", text, result_p2);
}
