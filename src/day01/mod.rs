use crate::lib::aoc;

pub fn run() {
    let lines = aoc::get_lines("./inputs/day1.in");

    let mut times_increased_1 = 0;
    let mut times_increased_2 = 0;
    let mut last: u32 = u32::MAX;
    // last3.0 and last3.1 can be 0 since we will never reach an actual sum of u32::MAX
    let mut last3: (u32, u32, u32) = (0, 0, u32::MAX);
    let mut last3_sum: u32 = sum_triple(last3);

    for line in lines {
        let current = line.parse::<u32>().unwrap();
        let current3 = (last3.1, last3.2, current);
        let current3_sum = sum_triple(current3);

        if current > last {
            times_increased_1 += 1;
        }

        if current3_sum > last3_sum {
            times_increased_2 += 1;
        }

        last = current;
        last3 = current3;
        last3_sum = current3_sum;
    }

    // Output
    aoc::output(1, "times increased", times_increased_1, times_increased_2);
}

fn sum_triple(triple: (u32, u32, u32)) -> u32 {
    if triple.0 == u32::MAX || triple.1 == u32::MAX || triple.2 == u32::MAX {
        u32::MAX
    } else {
        triple.0 + triple.1 + triple.2
    }
}
