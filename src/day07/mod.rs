use crate::lib::aoc;

pub fn run() {
    let mut crabs: Vec<i32> = aoc::get_lines("./inputs/day7.in")[0]
        .split(',')
        .map(|crab| crab.parse().expect("Couldn't parse some crab!"))
        .collect();
    crabs.sort_unstable();
    let crabs = crabs;

    // ~~~ Part 1 ~~~
    // Get the median's index
    let min_fuel_indices: (i32, Option<i32>) = match crabs.len() % 2 == 0 {
        // We have to do some extra math if there are an even number of crabs in submarines
        true => (crabs[crabs.len() / 2], Some(crabs[crabs.len() / 2 + 1])),
        false => (crabs[crabs.len() / 2], None),
    };

    let fuel_1 = crabs.iter().fold(0, |total_fuel_1, crab| {
        total_fuel_1 + (crab - min_fuel_indices.0).abs()
    });
    let fuel_2 = match min_fuel_indices.1 {
        Some(i) => crabs
            .iter()
            .fold(0, |total_fuel_1, crab| total_fuel_1 + (crab - i).abs()),
        None => i32::MAX,
    };

    let min_fuel_p1 = i32::min(fuel_1, fuel_2) as u32;

    // ~~~ Part 2 ~~~
    let max_crab = crabs
        .iter()
        .reduce(|current, next| match next > current {
            true => next,
            false => current,
        })
        .expect("No max crab! Did you forget to include the input?");

    // Brute force this shit
    let mut fuel_per_index: Vec<i32> = Vec::new();
    for i in 0..*max_crab {
        fuel_per_index.push(0);
        for crab in &crabs {
            let diff = (i - crab).abs();
            fuel_per_index[i as usize] += diff * (diff + 1) / 2;
        }
    }

    let min_fuel_p2 = fuel_per_index
        .iter()
        .reduce(|current, next| match next < current {
            true => next,
            false => current,
        })
        .expect("No min fuel index!????");

    aoc::output(7, "total fuel", min_fuel_p1, *min_fuel_p2 as u32);
}
