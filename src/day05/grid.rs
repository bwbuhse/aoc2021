use std::cmp::Ordering;

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
    UpRight,
    UpLeft,
    DownRight,
    DownLeft,
}

#[derive(Clone, Copy, Debug)]
pub struct Point(pub u32, pub u32);

#[derive(Clone, Copy, Debug)]
pub struct Line(pub Point, pub Point);

#[derive(Clone, Debug)]
pub struct Grid {
    grid: Vec<Vec<u32>>,
}

impl Point {
    fn from(x: u32, y: u32) -> Self {
        Point(x, y)
    }
}

impl Line {
    pub fn from(string: String) -> Self {
        let points = string
            .split(" -> ")
            .collect::<Vec<_>>()
            .iter()
            .map(|point| {
                point
                    .split(',')
                    .collect::<Vec<_>>()
                    .iter()
                    .map(|coord| {
                        coord
                            .parse::<u32>()
                            .expect("Could not parse some coordinate")
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        let p1 = points.get(0).expect("Didn't find point 1!");
        let p2 = points.get(1).expect("Didn't find point 2!");

        Line(
            Point::from(
                *p1.get(0).expect("Didn't find x1"),
                *p1.get(1).expect("Didn't find y1"),
            ),
            Point::from(
                *p2.get(0).expect("Didn't find x2"),
                *p2.get(1).expect("Didn't find y2"),
            ),
        )
    }

    fn get_direction(&self) -> Direction {
        // We're assuming lines can only be vertical or horizontal
        if self.0 .0 < self.1 .0 {
            match (self.0 .1).cmp(&self.1 .1) {
                Ordering::Greater => Direction::UpRight,
                Ordering::Less => Direction::DownRight,
                Ordering::Equal => Direction::Right,
            }
        } else if self.0 .0 > self.1 .0 {
            match (self.0 .1).cmp(&self.1 .1) {
                Ordering::Greater => Direction::UpLeft,
                Ordering::Less => Direction::DownLeft,
                Ordering::Equal => Direction::Left,
            }
        } else if self.0 .1 < self.1 .1 {
            Direction::Down
        } else if self.0 .1 > self.1 .1 {
            Direction::Up
        } else {
            panic!("Line doesn't have a direction!")
        }
    }
}

impl Grid {
    pub fn new(lines: Vec<Line>) -> Self {
        // let grid: Vec<Vec<u32>> = Vec::new();
        let maxes = find_maxes(&lines);

        let mut grid = vec![vec![0; maxes.0 as usize + 1]; maxes.1 as usize + 1];

        for line in lines {
            match line.get_direction() {
                Direction::Right => {
                    for i in line.0 .0..=line.1 .0 {
                        grid[line.0 .1 as usize][i as usize] += 1;
                    }
                }
                Direction::Left => {
                    for i in line.1 .0..=line.0 .0 {
                        grid[line.0 .1 as usize][i as usize] += 1;
                    }
                }
                Direction::Down => {
                    for i in line.0 .1..=line.1 .1 {
                        grid[i as usize][line.0 .0 as usize] += 1;
                    }
                }
                Direction::Up => {
                    for i in line.1 .1..=line.0 .1 {
                        grid[i as usize][line.0 .0 as usize] += 1;
                    }
                }
                Direction::UpRight => {
                    // "Right" uses - since our loop actually starts at the second coord
                    let mut x_coord = line.1 .0 as i32;
                    for i in line.1 .1..=line.0 .1 {
                        grid[i as usize][x_coord as usize] += 1;
                        x_coord -= 1;
                    }
                }
                Direction::UpLeft => {
                    // "Left" uses + since our loop actually starts at the second coord
                    let mut x_coord = line.1 .0 as i32;
                    for i in line.1 .1..=line.0 .1 {
                        grid[i as usize][x_coord as usize] += 1;
                        x_coord += 1;
                    }
                }
                Direction::DownRight => {
                    let mut x_coord = line.0 .0 as i32;
                    for i in line.0 .1..=line.1 .1 {
                        grid[i as usize][x_coord as usize] += 1;
                        x_coord += 1;
                    }
                }
                Direction::DownLeft => {
                    let mut x_coord = line.0 .0 as i32;
                    for i in line.0 .1..=line.1 .1 {
                        grid[i as usize][x_coord as usize] += 1;
                        x_coord -= 1;
                    }
                }
            }
        }

        Grid { grid }
    }

    pub fn count_dangerous(&self, threshold: u32) -> u32 {
        let mut dangerous = 0;
        for row in &self.grid {
            for column in row {
                if column >= &threshold {
                    dangerous += 1;
                }
            }
        }

        dangerous
    }
}

fn find_maxes(lines: &[Line]) -> (u32, u32) {
    let mut max_x = 0;
    let mut max_y = 0;

    for line in lines {
        if line.0 .0 > max_x {
            max_x = line.0 .0;
        }
        if line.0 .1 > max_y {
            max_y = line.0 .1;
        }

        if line.1 .0 > max_x {
            max_x = line.1 .0;
        }
        if line.1 .1 > max_y {
            max_y = line.1 .1;
        }
    }

    (max_x, max_y)
}
