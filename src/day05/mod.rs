mod grid;
use crate::day05::grid::Grid;
use crate::day05::grid::Line;
use crate::lib::aoc;

pub fn run() {
    let lines: Vec<Line> = aoc::get_lines("./inputs/day5.in")
        .iter()
        .map(|line| Line::from(line.to_string()))
        .collect();
    let mut lines_p1 = lines.clone();
    lines_p1.retain(|line| line.0 .0 == line.1 .0 || line.0 .1 == line.1 .1);

    let grid_p1: Grid = Grid::new(lines_p1);
    let grid_p2: Grid = Grid::new(lines);

    aoc::output(
        5,
        "dangerous spots",
        grid_p1.count_dangerous(2),
        grid_p2.count_dangerous(2),
    );
}
