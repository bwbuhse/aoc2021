use crate::lib::aoc;
use std::collections::HashSet;

pub fn run() {
    let input = &aoc::get_lines("./inputs/day17.in")[0];
    if !input.starts_with("target area: ") {
        panic!("Invalid input!");
    }

    // target area: x=20..30, y=-10..-5
    let input: Vec<&str> = input.split_whitespace().collect();
    let x_range: Vec<&str> = input[2].split("..").collect();
    let x_min = x_range[0][2..].to_owned();
    let x_max = x_range[1][0..x_range[1].len() - 1].to_owned();
    let x_min: i32 = x_min.parse().unwrap();
    let x_max: i32 = x_max.parse().unwrap();
    let y_range: Vec<&str> = input[3].split("..").collect();
    let y_min = y_range[0][2..].to_owned();
    let y_max = y_range[1][0..].to_owned();
    let y_min: i32 = y_min.parse().unwrap();
    let y_max: i32 = y_max.parse().unwrap();

    let target_x = x_min..=x_max;
    let target_y = y_min..=y_max;

    let initial_x_velocity_range = 0..=x_max;

    // For Part 1
    let mut max_y_pos = 0;
    // For Part 2
    let mut velocities_that_hit = HashSet::new();
    for initial_x_velocity in initial_x_velocity_range {
        for initial_y_velocity in y_min..1000 {
            let mut coords = (0, 0);
            let mut this_max_y_pos = 0;
            let mut x_velocity = initial_x_velocity;
            let mut y_velocity = initial_y_velocity;
            let mut hits_target = false;
            while coords.0 <= x_max && coords.1 >= y_min {
                coords.0 += x_velocity;
                coords.1 += y_velocity;
                if x_velocity > 0 {
                    x_velocity -= 1;
                }
                y_velocity -= 1;

                this_max_y_pos = i32::max(this_max_y_pos, coords.1);
                if target_x.contains(&coords.0) && target_y.contains(&coords.1) {
                    hits_target = true;
                    break;
                }
            }

            if hits_target {
                if this_max_y_pos >= max_y_pos {
                    max_y_pos = this_max_y_pos;
                }
                velocities_that_hit.insert((initial_x_velocity, initial_y_velocity));
            }
        }
    }

    aoc::output(
        17,
        "max y",
        max_y_pos as u32,
        velocities_that_hit.len() as u32,
    );
}
