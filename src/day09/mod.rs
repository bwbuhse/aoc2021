use crate::lib::aoc;
use std::collections::BinaryHeap;
use std::collections::HashSet;
use std::collections::VecDeque;

// Assumes that (row, col) are valid coordinates
fn get_neighbor_coords(
    heightmap: &[Vec<u32>],
    (row, col): (usize, usize),
) -> HashSet<(usize, usize)> {
    if row == 0 && col == 0 {
        // Top-left corner
        HashSet::from([(row + 1, col), (row, col + 1)])
    } else if row == heightmap.len() - 1 && col == 0 {
        // Bottom-left corner
        HashSet::from([(row - 1, col), (row, col + 1)])
    } else if row == 0 && col == heightmap[row].len() - 1 {
        // Top-right corner
        HashSet::from([(row + 1, col), (row, col - 1)])
    } else if row == heightmap.len() - 1 && col == heightmap[row].len() - 1 {
        // Bottom-right corner
        HashSet::from([(row - 1, col), (row, col - 1)])
    } else if row == 0 {
        // Top-row, not a corner
        HashSet::from([(row + 1, col), (row, col + 1), (row, col - 1)])
    } else if row == heightmap.len() - 1 {
        // Bottom-row, not a corner
        HashSet::from([(row - 1, col), (row, col + 1), (row, col - 1)])
    } else if col == 0 {
        // Left-column, not a corner
        HashSet::from([(row + 1, col), (row - 1, col), (row, col + 1)])
    } else if col == heightmap[row].len() - 1 {
        // Right-column, not a corner
        HashSet::from([(row + 1, col), (row - 1, col), (row, col - 1)])
    } else {
        // Not on any edge
        HashSet::from([
            (row + 1, col),
            (row - 1, col),
            (row, col + 1),
            (row, col - 1),
        ])
    }
}

fn get_basin_size(heightmap: &[Vec<u32>], (row, col): (usize, usize)) -> u32 {
    // Use a bfs???
    let mut queue: VecDeque<(usize, usize)> = VecDeque::from([(row, col)]);
    let mut have_been_explored: HashSet<(usize, usize)> = HashSet::from([(row, col)]);
    while !queue.is_empty() {
        let (row, col) = queue.pop_front().unwrap();
        // 9s are crests, i.e. they aren't in the basin
        if heightmap[row][col] == 9 {
            continue;
        }
        // Now check all edges
        for coord in get_neighbor_coords(heightmap, (row, col)) {
            if heightmap[coord.0][coord.1] != 9 && !have_been_explored.contains(&coord) {
                have_been_explored.insert(coord);
                queue.push_back(coord);
            }
        }
    }

    have_been_explored.len() as u32
}

pub fn run() {
    let heightmap: Vec<Vec<u32>> = aoc::get_lines("./inputs/day9.in")
        .iter()
        .map(|line| {
            line.chars().fold(Vec::new(), |mut accum, cur| {
                accum.push(cur.to_digit(10).expect("Failed to convert a char to u32!"));
                accum
            })
        })
        .collect();

    let mut low_points: HashSet<(usize, usize)> = HashSet::new();
    for row in 0..heightmap.len() {
        for col in 0..heightmap[row].len() {
            let neighbor_coords = get_neighbor_coords(&heightmap, (row, col));
            let is_low_point =
                neighbor_coords
                    .iter()
                    .fold(true, |is_low_point, (neighbor_row, neighbor_col)| {
                        is_low_point
                            && heightmap[row][col] < heightmap[*neighbor_row][*neighbor_col]
                    });
            if is_low_point {
                low_points.insert((row, col));
            }
        }
    }

    let low_point_risk_values: Vec<u32> = low_points
        .iter()
        .map(|(row, col)| heightmap[*row][*col] + 1)
        .collect();

    let low_point_risk_values_sum = low_point_risk_values.iter().sum();

    let mut basin_sizes: BinaryHeap<u32> = BinaryHeap::new();
    for low_point in low_points {
        basin_sizes.push(get_basin_size(&heightmap, low_point));
    }

    let multiplicand_largest_3_basins =
        basin_sizes.pop().unwrap() * basin_sizes.pop().unwrap() * basin_sizes.pop().unwrap();

    aoc::output(
        9,
        "sum",
        low_point_risk_values_sum,
        multiplicand_largest_3_basins,
    );
}
