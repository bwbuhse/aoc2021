mod cavern;
use crate::lib::aoc;
use cavern::Cavern;

pub fn run() {
    let cavern_p1 = Cavern::from(aoc::get_lines("./inputs/day15.in"));
    let mut cavern_p2 = cavern_p1.clone();
    cavern_p2.expand(5);

    let shortest_path_risk_level_p1 = cavern_p1.dijsktra().unwrap_or(0);
    let shortest_path_risk_level_p2 = cavern_p2.dijsktra().unwrap_or(0);

    aoc::output(
        15,
        "risk level",
        shortest_path_risk_level_p1,
        shortest_path_risk_level_p2,
    );
}
