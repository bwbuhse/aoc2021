use std::{
    cmp::{Ordering, Reverse},
    collections::{BinaryHeap, HashMap},
    fmt::{Display, Formatter, Result},
};

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
struct Node {
    coords: (usize, usize),
    risk_level: u32,
}

impl Node {
    fn new(row: usize, col: usize, risk_level: u32) -> Self {
        Node {
            coords: (row, col),
            risk_level,
        }
    }

    fn get_neighbor_cords(&self, num_rows: usize, num_cols: usize) -> Vec<(usize, usize)> {
        let mut neighor_coords = vec![];

        let (row, col) = self.coords;

        if row > 0 {
            neighor_coords.push((row - 1, col));
        }
        if row < num_rows - 1 {
            neighor_coords.push((row + 1, col));
        }
        if col > 0 {
            neighor_coords.push((row, col - 1));
        }
        if col < num_cols - 1 {
            neighor_coords.push((row, col + 1));
        }

        neighor_coords
    }
}

impl Display for Node {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}", self.risk_level)
    }
}

#[derive(Debug)]
struct Visit<V> {
    node: V,
    path_risk_level: u32,
}

impl<V> Visit<V> {
    fn new(node: V, path_risk_level: u32) -> Self {
        Visit {
            node,
            path_risk_level,
        }
    }
}

impl<V> Ord for Visit<V> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.path_risk_level.cmp(&other.path_risk_level)
    }
}

impl<V> PartialOrd for Visit<V> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<V> PartialEq for Visit<V> {
    fn eq(&self, other: &Self) -> bool {
        self.path_risk_level.eq(&other.path_risk_level)
    }
}

impl<V> Eq for Visit<V> {}

#[derive(Clone, Debug)]
pub struct Cavern {
    cavern: Vec<Vec<Node>>,
    pub num_rows: usize,
    pub num_cols: usize,
}

impl From<Vec<String>> for Cavern {
    fn from(lines: Vec<String>) -> Self {
        let mut cavern = Vec::new();

        for row in 0..lines.len() {
            cavern.push(Vec::new());
            let row_risk_levels: Vec<u32> =
                lines[row].chars().fold(Vec::new(), |mut accum, cur| {
                    accum.push(
                        cur.to_digit(10)
                            .expect("Failed to convert some risk level to u32!"),
                    );
                    accum
                });
            for (col, risk_level) in row_risk_levels.iter().enumerate() {
                cavern[row].push(Node::new(row, col, *risk_level));
            }
        }

        if cavern.is_empty() {
            panic!("Cavern must have at least 1 row!");
        }

        let num_rows = cavern.len();
        let num_cols = cavern[0].len();

        Cavern {
            cavern,
            num_rows,
            num_cols,
        }
    }
}

impl Cavern {
    pub fn expand(&mut self, num_expansions: usize) {
        /// Increment the base cavern. Risk levels wrap around from 9 back to 1
        fn increment_cavern(cavern: Vec<Vec<Node>>) -> Vec<Vec<Node>> {
            let mut new_cavern = Vec::new();

            for row in cavern {
                let mut new_row = Vec::new();
                for Node {
                    coords: (row, col),
                    risk_level,
                } in row
                {
                    let new_risk_level = risk_level + 1;
                    match new_risk_level > 9 {
                        true => new_row.push(Node::new(row, col, (risk_level + 1) % 9)),
                        false => new_row.push(Node::new(row, col, new_risk_level)),
                    };
                }
                new_cavern.push(new_row);
            }

            new_cavern
        }

        let mut new_cavern = vec![vec![]; num_expansions * self.num_rows];
        let mut expanded_caverns = HashMap::new();
        expanded_caverns.insert(0, self.cavern.clone());
        for i in 0..((num_expansions - 1) * 2) {
            expanded_caverns.insert(
                i + 1,
                increment_cavern(expanded_caverns.get(&i).unwrap().clone()),
            );
        }
        for i in 0..num_expansions {
            for j in i..(i + num_expansions) {
                for k in 0..self.cavern.len() {
                    let appendage = expanded_caverns.get(&j).unwrap()[k].clone();
                    let mut new_appendage = vec![];
                    // Need update the indices in the new nodes
                    for (l, old_node) in appendage.iter().enumerate() {
                        new_appendage.push(Node::new(
                            k + i * self.num_rows,
                            l + j * self.num_cols - (i * self.num_rows),
                            old_node.risk_level,
                        ));
                    }
                    new_cavern[k + i * self.num_rows].append(&mut new_appendage);
                }
            }
        }

        self.cavern = new_cavern;
        self.num_rows *= num_expansions;
        self.num_cols *= num_expansions;
    }

    pub fn dijsktra(&self) -> Option<u32> {
        let mut path_risk_levels = HashMap::new();
        let mut heap = BinaryHeap::new();

        let start = self.cavern[0][0];
        path_risk_levels.insert(start, start.risk_level);
        heap.push(Reverse(Visit::new(start, 0)));

        while let Some(Reverse(Visit {
            node,
            path_risk_level,
        })) = heap.pop()
        {
            if node.coords == (self.num_rows - 1, self.num_cols - 1) {
                return Some(path_risk_level);
            }
            for (row, col) in node.get_neighbor_cords(self.num_rows, self.num_cols) {
                let neighbor_node = self.cavern[row][col];
                let alt_distance = path_risk_level + neighbor_node.risk_level;
                let is_less_dangerous = path_risk_levels
                    .get(&neighbor_node)
                    .map_or(true, |&current| alt_distance < current);

                if is_less_dangerous {
                    path_risk_levels.insert(neighbor_node, alt_distance);
                    heap.push(Reverse(Visit::new(neighbor_node, alt_distance)));
                }
            }
        }

        None
    }
}

impl Display for Cavern {
    fn fmt(&self, f: &mut Formatter) -> Result {
        if let Err(e) = writeln!(
            f,
            "Num rows: {}, Num cols: {}, Cavern: ",
            self.num_rows, self.num_cols
        ) {
            return Err(e);
        }
        for row in &self.cavern {
            for node in row {
                if let Err(e) = write!(f, "{}", node) {
                    return Err(e);
                }
            }
            if let Err(e) = writeln!(f) {
                return Err(e);
            }
        }
        writeln!(f)
    }
}
