use std::fmt::{Display, Formatter, Result};

/*
 * The code for today is copied almost word for word from
 * https://tildes.net/~comp.advent_of_code/zot/day_18_snailfish#comment-6zl7
 *
 * I just really didn't like today's problem...
 * */

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SnailfishNumber {
    Open,
    Comma,
    Value(u32),
    Close,
}

impl Display for SnailfishNumber {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            SnailfishNumber::Open => write!(f, "["),
            SnailfishNumber::Comma => write!(f, ","),
            SnailfishNumber::Value(value) => write!(f, "{}", value),
            SnailfishNumber::Close => write!(f, "]"),
        }
    }
}

pub fn add(a: Vec<SnailfishNumber>, b: Vec<SnailfishNumber>) -> Vec<SnailfishNumber> {
    let mut result = Vec::new();
    result.push(SnailfishNumber::Open);
    result.extend(a.into_iter());
    result.push(SnailfishNumber::Comma);
    result.extend(b.into_iter());
    result.push(SnailfishNumber::Close);

    reduce(result)
}

pub fn reduce(s: Vec<SnailfishNumber>) -> Vec<SnailfishNumber> {
    let mut s = s;

    loop {
        let (exploded, has_exploded) = explode(s);
        s = exploded;

        if has_exploded {
            continue;
        }

        let (split, has_split) = split(s);
        s = split;

        if has_split {
            continue;
        }

        break;
    }

    s
}

fn explode(s: Vec<SnailfishNumber>) -> (Vec<SnailfishNumber>, bool) {
    // increase_by finds the first Value in the vector and increments it
    fn increase_by(s: &mut Vec<SnailfishNumber>, inc: u32) {
        for i in 0..s.len() {
            if let SnailfishNumber::Value(x) = s[i] {
                s[i] = SnailfishNumber::Value(x + inc);
                break;
            }
        }
    }

    // increase_by finds the last Value in the vector and increments it
    fn increase_by_rev(s: &mut Vec<SnailfishNumber>, inc: u32) {
        for i in (0..s.len()).rev() {
            if let SnailfishNumber::Value(x) = s[i] {
                s[i] = SnailfishNumber::Value(x + inc);
                break;
            }
        }
    }

    let mut depth = 0;

    for (idx, &c) in s.iter().enumerate() {
        match c {
            SnailfishNumber::Open => depth += 1,
            SnailfishNumber::Close => depth -= 1,
            SnailfishNumber::Comma => (),
            SnailfishNumber::Value(_) => {
                // If we are nested 4 deep...
                if depth > 4 {
                    // ...and this is not a sub-pair, ...
                    if s[idx + 2] == SnailfishNumber::Open {
                        continue;
                    }

                    let (prev, s2) = s.split_at(idx);
                    let mut prev = prev.to_vec();
                    prev.pop();

                    // ... extract the left and right numbers of the pair and...
                    if let SnailfishNumber::Value(left) = s2[0] {
                        if let SnailfishNumber::Value(right) = s2[2] {
                            // Split after the pair
                            let (_, trail) = s2.split_at(4);
                            let mut trail = trail.to_vec();

                            // Increase the last number in the prefix
                            increase_by_rev(&mut prev, left);

                            // Increase the postfix
                            increase_by(&mut trail, right);

                            // Concatenate everything
                            prev.push(SnailfishNumber::Value(0));
                            prev.extend(trail.iter());

                            return (prev, true);
                        }
                    }

                    unreachable!()
                }
            }
        }
    }

    (s, false)
}

fn split(s: Vec<SnailfishNumber>) -> (Vec<SnailfishNumber>, bool) {
    let mut result = Vec::new();

    for (idx, &c) in s.iter().enumerate() {
        if let SnailfishNumber::Value(num) = c {
            if num > 9 {
                let left = num / 2;
                let right = (num + 1) / 2;
                let (_, trail) = s.split_at(idx + 1);

                result.push(SnailfishNumber::Open);
                result.push(SnailfishNumber::Value(left));
                result.push(SnailfishNumber::Comma);
                result.push(SnailfishNumber::Value(right));
                result.push(SnailfishNumber::Close);
                result.extend(trail.into_iter());

                return (result, true);
            } else {
                result.push(c);
            }
        } else {
            result.push(c);
        }
    }

    (s, false)
}

pub fn magnitude(s: Vec<SnailfishNumber>) -> u32 {
    let mut stack = Vec::new();

    for c in s.iter() {
        match c {
            SnailfishNumber::Value(x) => stack.push(*x),
            SnailfishNumber::Comma => (),
            SnailfishNumber::Open => (),
            SnailfishNumber::Close => {
                let right = stack.pop().unwrap();
                let left = stack.pop().unwrap();
                let magnitude = 3 * left + 2 * right;
                stack.push(magnitude);
            }
        }
    }

    stack.pop().unwrap()
}
