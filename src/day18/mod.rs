mod snailfish_math;
use crate::lib::aoc;
use snailfish_math::SnailfishNumber;

/*
 * The code for today is copied almost word for word from
 * https://tildes.net/~comp.advent_of_code/zot/day_18_snailfish#comment-6zl7
 *
 * I just really didn't like today's problem...
 * */

fn parse(input: String) -> Vec<SnailfishNumber> {
    let mut result = Vec::new();

    for c in input.trim().chars() {
        result.push(match c {
            '[' => SnailfishNumber::Open,
            ']' => SnailfishNumber::Close,
            ',' => SnailfishNumber::Comma,
            // Parsing with base 16 simplifies the test-cases with numbers > 9,
            // since you can just specify them in hexadecimal
            _ => SnailfishNumber::Value(c.to_digit(16).unwrap() as u32),
        })
    }

    result
}

pub fn run() {
    let lines = aoc::get_lines("./inputs/day18.in");

    let snailfish_numbers: Vec<Vec<SnailfishNumber>> = lines
        .iter()
        .filter_map(|line| Some(parse(line.to_owned())))
        .collect();

    // Part 1
    let result_p1 = snailfish_math::magnitude(
        snailfish_numbers
            .clone()
            .into_iter()
            .reduce(|a, b| snailfish_math::add(a, b))
            .unwrap(),
    );

    // Part 2
    let mut result_p2 = 0;

    for i in 0..snailfish_numbers.len() - 1 {
        for j in i + 1..snailfish_numbers.len() {
            let first_magnitude = snailfish_math::magnitude(snailfish_math::add(
                snailfish_numbers[i].clone(),
                snailfish_numbers[j].clone(),
            ));
            let second_magnitude = snailfish_math::magnitude(snailfish_math::add(
                snailfish_numbers[j].clone(),
                snailfish_numbers[i].clone(),
            ));
            result_p2 = result_p2.max(first_magnitude);
            result_p2 = result_p2.max(second_magnitude);
        }
    }

    aoc::output(18, "result", result_p1, result_p2);
}
