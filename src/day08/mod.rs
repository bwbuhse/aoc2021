use crate::lib::aoc;
use std::collections::HashMap;
use std::collections::HashSet;

pub fn run() {
    let lines = aoc::get_lines("./inputs/day8.in");
    let (patterns, outputs) = lines
        .iter()
        .fold((Vec::new(), Vec::new()), |mut accum, line| {
            let split = line.split_once(" | ").expect("No pipe in your line!");
            accum.0.push(split.0);
            accum.1.push(split.1);
            accum
        });

    // Part 1
    let mut count_uniques = 0;
    for line in &outputs {
        for digit in line.split_whitespace() {
            match digit.len() {
                2 | 3 | 4 | 7 => count_uniques += 1,
                _ => {}
            }
        }
    }

    // Part 2
    let mut sum_outputs: u32 = 0;
    for i in 0..lines.len() {
        // Map of segments to possible wires
        let mut possible: HashMap<char, HashSet<char>> = HashMap::new();
        // Map of segments to confirmed wires
        let mut confirmed: HashMap<char, char> = HashMap::new();
        // Map of a digit to its wires
        let mut digits: HashMap<u32, HashSet<char>> = HashMap::new();
        // Set of len 5 patterns
        let mut fives: HashSet<Vec<char>> = HashSet::new();
        // Set of len 6 patterns
        let mut sixes: HashSet<Vec<char>> = HashSet::new();

        for pattern in patterns[i].trim().split_whitespace() {
            // wires used for this digit
            let current_wires: HashSet<char> =
                pattern.chars().fold(HashSet::new(), |mut accum, cur| {
                    accum.insert(cur);
                    accum
                });
            match pattern.len() {
                2 => {
                    // len == 2 is unique to the 1-digit
                    let new_set = match possible.get(&'c') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('c', new_set);
                    let new_set = match possible.get(&'f') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('f', new_set);

                    digits.insert(1, current_wires);
                }
                3 => {
                    // len == 3 is unique to the 7-digit
                    let new_set = match possible.get(&'a') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('a', new_set);

                    let new_set = match possible.get(&'c') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('c', new_set);
                    let new_set = match possible.get(&'f') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('f', new_set);

                    digits.insert(7, current_wires);
                }
                4 => {
                    // len == 4 is unique to the 4-digit
                    let new_set = match possible.get(&'b') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('b', new_set);

                    let new_set = match possible.get(&'c') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('c', new_set);

                    let new_set = match possible.get(&'d') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('d', new_set);
                    let new_set = match possible.get(&'f') {
                        Some(set) => set.intersection(&current_wires.clone()).copied().collect(),
                        None => current_wires.clone(),
                    };
                    possible.insert('f', new_set);

                    digits.insert(4, current_wires);
                }
                5 => {
                    // len == 5 can be a 2, 3, or 5
                    fives.insert(current_wires.iter().copied().collect());
                }
                6 => {
                    // len == 6 can be a 0, 6, or 9
                    sixes.insert(current_wires.iter().copied().collect());
                }
                7 => {
                    // len == 7 is unique to the 8-digit
                    digits.insert(8, current_wires);
                }
                _ => { /* We should not reach here */ }
            }
        }
        // Do deduction
        // Find the wire that matches to segment a
        let one_digit = digits.get(&1).unwrap().clone();
        let seven_digit = digits.get(&7).unwrap().clone();
        let mut a_wire: char = 'z';
        for wire in &seven_digit {
            match &one_digit.contains(wire) {
                true => {}
                false => a_wire = *wire,
            }
        }
        confirmed.insert('a', a_wire);

        // Find the 3 options for segments b, f, and g
        let mut sixes_iter = sixes.iter();
        let mut sixes_intersection = sixes_iter.next().unwrap().clone();
        sixes_intersection.retain(|wire| !confirmed.values().any(|x| x == wire));
        for pattern in sixes_iter {
            sixes_intersection.retain(|wire| pattern.contains(wire));
        }

        // Find the 2 options for segment d
        let mut fives_iter = fives.iter();
        let mut fives_intersection = fives_iter.next().unwrap().clone();
        fives_intersection.retain(|wire| !confirmed.values().any(|x| x == wire));
        for pattern in fives_iter {
            fives_intersection.retain(|wire| pattern.contains(wire));
        }

        // Get segment d
        let four_digit = digits.get(&4).unwrap().clone();
        let mut d_wire: char = 'z';
        for wire in &four_digit {
            if !&one_digit.contains(wire) && fives_intersection.contains(wire) {
                d_wire = *wire;
                break;
            }
        }
        confirmed.insert('d', d_wire);

        // Get segment b
        let mut b_wire: char = 'z';
        for wire in &four_digit {
            if !&one_digit.contains(wire) && *wire != d_wire {
                b_wire = *wire;
            }
        }
        confirmed.insert('b', b_wire);

        // Find wires for digit 0
        let mut zero_digit = HashSet::new();
        for digit in &sixes {
            if !digit.contains(&d_wire) {
                zero_digit = digit.iter().copied().collect();
            }
        }
        digits.insert(0, zero_digit.clone());

        // Find wires for digit 6
        let mut six_digit = HashSet::new();
        for digit in &sixes {
            let digit: HashSet<char> = digit.iter().copied().collect();
            if digit != zero_digit
                && digit
                    .iter()
                    .fold(0, |accum, wire| match &one_digit.contains(wire) {
                        true => accum + 1,
                        false => accum,
                    })
                    == 1
            {
                six_digit = digit;
            }
        }
        digits.insert(6, six_digit.clone());

        // Find wires for digit 9
        let mut nine_digit = HashSet::new();
        for digit in &sixes {
            let digit: HashSet<char> = digit.iter().copied().collect();
            if digit != zero_digit && digit != six_digit {
                nine_digit = digit;
            }
        }
        digits.insert(9, nine_digit.clone());

        // Find wires for digit 3
        let mut three_digit = HashSet::new();
        for digit in &fives {
            let digit: HashSet<char> = digit.iter().copied().collect();
            if digit
                .iter()
                .fold(0, |accum, wire| match &one_digit.contains(wire) {
                    true => accum + 1,
                    false => accum,
                })
                == 2
            {
                three_digit = digit;
            }
        }
        digits.insert(3, three_digit.clone());

        // Find wires for digits 2 and 5
        let mut five_digit = HashSet::new();
        let mut two_digit = HashSet::new();
        for digit in &fives {
            let digit: HashSet<char> = digit.iter().copied().collect();
            if digit != three_digit && digit.contains(&b_wire) {
                five_digit = digit.clone();
            }
            if digit != three_digit && !digit.contains(&b_wire) {
                two_digit = digit.clone();
            }
        }
        digits.insert(5, five_digit.clone());
        digits.insert(2, two_digit.clone());

        // Get the sum for this line
        let mut output_value = 0;
        let eight_digit = digits.get(&8).unwrap().clone();
        for digit in outputs[i].split_whitespace() {
            output_value *= 10;
            let digit = digit.chars().fold(HashSet::new(), |mut accum, cur| {
                accum.insert(cur);
                accum
            });
            if digit == one_digit {
                output_value += 1;
            } else if digit == two_digit {
                output_value += 2;
            } else if digit == three_digit {
                output_value += 3;
            } else if digit == four_digit {
                output_value += 4;
            } else if digit == five_digit {
                output_value += 5;
            } else if digit == six_digit {
                output_value += 6;
            } else if digit == seven_digit {
                output_value += 7;
            } else if digit == eight_digit {
                output_value += 8;
            } else if digit == nine_digit {
                output_value += 9;
            }
        }
        sum_outputs += output_value;
    }

    aoc::output(8, "count", count_uniques, sum_outputs);
}
